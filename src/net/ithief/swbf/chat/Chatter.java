package net.ithief.swbf.chat;

import net.ithief.swbf.chat.GUI.ChatGUI;
import net.ithief.swbf.memory.BattleFront;
import net.ithief.swbf.memory.BattleFront.StatsMessageType;
import net.ithief.swbf.memory.MessageListener;
import net.ithief.swbf.memory.exeptions.BattlefrontNotRunningException;

public class Chatter extends ChatGUI {

	private static final long serialVersionUID = -6311968419222161863L;
	private BattleFront swbf;

	public Chatter() {
		try {
			swbf = new BattleFront();
			swbf.setMessageReceiveListener(new MessageListener() {

				@Override
				public void messageSend(boolean team, String message) {
					addChatMessage(team, message);
				}

				@Override
				public void statusMessage(StatsMessageType type, String message) {
					addStatusMessage(type, message);
				}

			});
		} catch (BattlefrontNotRunningException e) {
			error("SWBF is not running!\nStart the game and restart this application.", true);
		}
	}

	@Override
	protected void detach() {
		if (swbf != null) {
			try {
				swbf.detach();
			} catch (Throwable e) {
				error("SWBF detachment failed.\nThe game will exit.\n(This is somehow systems fault)", true);
			}
		}
	}

	@Override
	protected void exit() {
		System.out.println("Exiting");
		System.exit(1);
	}

}
