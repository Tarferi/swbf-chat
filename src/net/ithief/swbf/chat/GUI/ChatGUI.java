package net.ithief.swbf.chat.GUI;

import javax.swing.JFrame;

import java.awt.BorderLayout;

import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;

import net.ithief.swbf.memory.BattleFront.StatsMessageType;

import java.awt.Font;
import java.awt.Color;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public abstract class ChatGUI extends JFrame {

	private static final long serialVersionUID = 6393439792825097811L;
	private JTextPane chatContent;
	private Style teamChat;
	private Style normalChat;
	private Document doc;
	private Style uniStyle;
	private Style enemyChat;
	private Style unknownChat;

	public ChatGUI() {
		setSize(640, 400);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				detach();
				exit();
			}

		});
		setTitle("SWBF chat");
		getContentPane().setLayout(new BorderLayout(0, 0));

		JScrollPane scrollPane = new JScrollPane();
		getContentPane().add(scrollPane, BorderLayout.CENTER);

		chatContent = new JTextPane() {
			private static final long serialVersionUID = -772274729001072807L;

			@Override
			public boolean getScrollableTracksViewportWidth() {
				return true;
			}
		};
		chatContent.setEditable(false);
		chatContent.setForeground(Color.YELLOW);
		chatContent.setBackground(Color.BLACK);
		chatContent.setFont(new Font("Consolas", Font.PLAIN, 18));
		scrollPane.setViewportView(chatContent);
		setVisible(true);
		Style DefaultStyle = chatContent.addStyle("defaultStyle", null);

		StyleConstants.setBackground(DefaultStyle, Color.black);
		StyleConstants.setFontFamily(DefaultStyle, "Consolas");
		StyleConstants.setFontSize(DefaultStyle, 18);

		teamChat = chatContent.addStyle("teamchat", DefaultStyle);
		normalChat = chatContent.addStyle("normalchat", DefaultStyle);
		uniStyle = chatContent.addStyle("normalchat", DefaultStyle);
		enemyChat = chatContent.addStyle("enemychat", DefaultStyle);
		unknownChat = chatContent.addStyle("unknownChat", DefaultStyle);

		StyleConstants.setForeground(teamChat, Color.green);
		StyleConstants.setForeground(normalChat, Color.yellow);
		StyleConstants.setForeground(uniStyle, Color.white);
		StyleConstants.setForeground(enemyChat, Color.red);
		StyleConstants.setForeground(unknownChat, Color.blue);

		doc = chatContent.getDocument();
	}

	protected void error(String message, boolean exit) {
		JOptionPane.showMessageDialog(this, message);
		if (exit) {
			try {
				detach();
			} catch (Throwable e) {
			}
			exit();
		}
	}

	protected abstract void exit();

	protected abstract void detach();

	private static final SimpleDateFormat date = new SimpleDateFormat("HH:MM:ss");

	public void addStatusMessage(StatsMessageType type, String message) {
		if (type == StatsMessageType.ALLY) {
			addMessage(formatMessage(message), teamChat);
		} else if (type == StatsMessageType.ENEMY) {
			addMessage(formatMessage(message), enemyChat);
		} else if (type == StatsMessageType.NEUTRAL) {
			addMessage(formatMessage(message), uniStyle);
		} else if (type == StatsMessageType.UNKNOWN) {
			addMessage(formatMessage(message), unknownChat);
		}
	}

	public void addChatMessage(boolean team, String message) {
		addMessage(formatMessage(message), team ? teamChat : normalChat);
	}

	private static String formatMessage(String message) {
		return message = "[" + date.format(new Date()) + "] " + message + "\n";
	}

	private void addMessage(String message, Style style) {
		int doclen = doc.getLength();
		try {
			doc.insertString(doclen, message, style);
			if (doclen - chatContent.getCaretPosition() < 20) {
				chatContent.setCaretPosition(doclen);
			}
		} catch (BadLocationException e) {
		}
	}

}
